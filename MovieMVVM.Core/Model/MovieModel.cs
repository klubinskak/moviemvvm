﻿using System;
using MvvmCross.ViewModels;

namespace MovieMVVM.Core.Model
{
    public class MovieModel : MvxViewModel
    {
       public string title { get; set; }
       public string date { get; set; }
       public string image { get; set; }
    }
}
