﻿using System;
using MovieMVVM.Core.Model;
using MovieMVVM.Core.Service;
using MovieMVVM.Core.ViewModel;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;

namespace MovieMVVM.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.RegisterType<MovieModel>();


            RegisterAppStart<MovieViewModel>();
        }
    }
}
