﻿using System;
using MovieMVVM.Core.Model;
using MvvmCross.ViewModels;

namespace MovieMVVM.Core.ViewModel
{
    public class MovieViewModel : MvxViewModel
    {
        private MvxObservableCollection<MovieModel> _movies;
        public MvxObservableCollection<MovieModel> Movies
        {
            get => _movies;
            set
            {
                _movies = value;
                RaisePropertyChanged(() => Movies);
            }
        }
    }
}
