﻿using System;
using Android.App;
using Android.Runtime;
using MovieMVVM.Core;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.Platforms.Android.Views;

namespace MovieMVVM.Android
{
    [Application]
    public class MainApplication : MvxAndroidApplication<MvxAndroidSetup<App>, App>
    {
        public MainApplication(IntPtr javaReference, JniHandleOwnership transfer)
            :base (javaReference, transfer)
        {
        }
    }
}
