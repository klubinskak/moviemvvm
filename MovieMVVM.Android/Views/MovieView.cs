﻿using System;
using Android.App;
using Android.OS;
using MovieMVVM.Core.ViewModel;
using MvvmCross.Platforms.Android.Views;

namespace MovieMVVM.Android.Views
{
    [Activity(Label = "Movies", MainLauncher = true)]
    public class MovieView : MvxActivity<MovieViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.mvx_recycler_view);
        }

    }
}
